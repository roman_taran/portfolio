import { combineReducers } from 'redux'
import { routerReducer }   from 'react-router-redux'

import openMenuReducer from './openMenu'


export default combineReducers({
	// count: counterReducer,
	openMenu: openMenuReducer,
	routing: routerReducer
})