import { CLOSE_MENU, OPEN_MENU } from '../constants'

export default  (open = false, action) => {
	const {type} = action;
	if (type === OPEN_MENU) {
		return !open
	}
	if ( type === CLOSE_MENU ) {
		return false
	}
	return open
}