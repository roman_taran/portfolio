import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools }          from 'redux-devtools-extension';

import reduser              from '../reduser'
import createBrowserHistory from "history/createBrowserHistory";
import { routerMiddleware } from "react-router-redux";


export const history = createBrowserHistory();
const middleware = routerMiddleware(history);

const composeEnhancers = composeWithDevTools({
	// Specify here name, actionsBlacklist, actionsCreators and other options

});

export const store = createStore(reduser, composeEnhancers(
	applyMiddleware(middleware)
));
