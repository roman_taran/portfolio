import React from 'react';

/*custom component*/
import Hero                 from '../components/Hero'
import PortfolioSection     from "../components/Portfolio";
import ContactForm          from "../components/ContactForm";
import Header   from '../layout/Header'

let Portfolio = () => {

	return (
		<main>
			<Header />
			<Hero>Личный сайт веб разработчика</Hero>
			<PortfolioSection/>
			<ContactForm/>
		</main>
	);
};

export default Portfolio;
