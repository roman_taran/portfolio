import React, { Component } from 'react'
import CSSTransitionGroup  from 'react-addons-css-transition-group'


/*custom*/
import LoginContainer from '../components/LoginContainer'

/*img*/
import bg from '../assets/images/water.jpg'


class Login extends Component {
    state = {
        backActive: false
    };

    handleClick = (e) => {
        this.setState({
            backActive: !this.state.backActive,
        })
    };

    componentDidMount() {
        this.main.classList.add(this.activeClass());
    };

    activeClass = () => this.state.backActive ? 'flip-back-active' : 'flip-front-active';

    render() {
        const styleLogin = { backgroundImage: `url(${bg})` };
        return (
            <main className={ this.activeClass() } ref={e => this.main = e }>
                <div className="login" style={styleLogin}>
                    <CSSTransitionGroup
                        transitionName="login-btn"
                        transitionEnterTimeout={300}
                        transitionLeaveTimeout={150}>

                        {!this.state.backActive ?
                            <div className="container-fluid">
                                <div className="login_btn">
                                    <button className="btn--transparent"
                                            onClick={this.handleClick}>
                                        Авторизоватся
                                    </button>
                                </div>
                            </div> : '' }

                    </CSSTransitionGroup>
                    <div className="login_inner">
                        <LoginContainer backActive={this.state.backActive} handleClick={this.handleClick}/>
                    </div>
                </div>
            </main>
        );
    }

};

export default Login;

