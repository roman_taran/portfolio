import React      from 'react';

/*data*/
import { skills } from '../api/Skills'

/*custom component*/
import Hero       from "../components/Hero";
import About      from "../components/About";
import Gmap       from "../components/GMap";
import Header     from '../layout/Header'


let Home = () => {

	return (
		<main>
			<Header/>
			<Hero>
				Личный сайт веб разработчика
			</Hero>
			<About skillData={skills}/>
			<Gmap/>
		</main>
	);
}

export default Home;
