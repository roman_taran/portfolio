import React    from 'react';

/*custom component*/
import Hero     from '../components/Hero'
import Header   from '../layout/Header'


let Blog = () => {

	return (
		<main>
			<Hero>
				<Header/>
			</Hero>
		</main>
	);
};

export default Blog;
