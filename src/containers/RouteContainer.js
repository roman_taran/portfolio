import React, { Component } from 'react'


export default class RouteContainer extends Component {
	constructor(props) {
		super(props);

		this.props.history.listen((location, action) => {
			// console.log("on route change");
			this.props.closeMenu()
		});
	}

	render() {
		return (
			<div>
				{this.props.children}
			</div>
		)
	}
}