import React, { Component } from 'react'

/*custom*/
import Socials              from '../../components/Socials'
import Nav                  from '../../components/Nav'


export default class Footer extends Component {

	render() {
		return (
			<footer className="footer">
				<div className="container-fluid">
					<div className="footer_inner">
						<div className="footer_top">
							<div className="footer_nav">
								<Nav className={'nav--row'}/>
							</div>
							<div className="footer_socials">
								<Socials className={'socials--border'}/>
							</div>
						</div>
					</div>
				</div>
				<hr/>
				<div className="container-fluid">
					<div className="footer_inner">
						<div className="footer_bottom">
							<p>
								© Роман Блудов | 2018
							</p>
						</div>
					</div>
				</div>
			</footer>
		)
	}
}