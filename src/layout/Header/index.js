import React, { Component } from 'react'

/*custom components*/
import Socials              from '../../components/Socials'
import Sandwich             from '../../components/Sandwich'


export default class Header extends Component {

    render() {

        return (
            <header className="header">
                <div className="container-fluid">
                    <div className="header_inner">
                        <div className="header_social">
                            <Socials />
                        </div>
                        <div className="header_menu">
                        </div>
                        <Sandwich />
                    </div>

                </div>
            </header>
        );
    }
}

// Header.propTypes = {
// 	title: PropTypes.string
// };
