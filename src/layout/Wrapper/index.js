import React, { Component } from 'react'
import { Route }            from 'react-router-dom'
import CSSTransitionGroup  from 'react-addons-css-transition-group'
import {TransitionGroup, CSSTransition } from "react-transition-group";

/*layout*/
import Footer               from "../../layout/Footer";

/*containers*/
import Home                 from "../../containers/Home";
import Blog                 from '../../containers/Blog'
import Portfolio            from '../../containers/Portfolio'
import Login                from '../../containers/Login'
import ContainerNav         from "../../components/Nav/ContainerNav"
import Container            from '../../containers'


class Wrapper extends Component {
	constructor(props) {
		super(props);

		// this.state = {
		// 	location: this.props.location,
		// }

	}

	render() {
		// console.log(this.state.location);
		return (
			<div className="App">
				<Container>
                    <TransitionGroup className="csstra">
                        <CSSTransition key={12} classNames="fade" timeout={300} >
                            <div>
                                <ContainerNav/>
                                <Route exact path='/' component={Home}/>
                                <Route path='/blog' component={Blog}/>
                                <Route path='/portfolio' component={Portfolio}/>
                                <Route path='/login' component={Login}/>
                                <Footer/>
                            </div>
                        </CSSTransition>

                    </TransitionGroup>

				</Container>

			</div>
		);
	}
};

// function mapStateToProps(state) {
//     console.log(state)
//
// 	return {
// 		location: state.routing.location
// 	};
// }

// export default connect(mapStateToProps)(Wrapper);
export default Wrapper;

