import React, {Component} from 'react'

export default Component => class OpenComponent extends Component {
	state = {
		openComponent: false
	}

	render() {
		return (
				<Component {...this.props} toggleOpenComponent={this.toggleOpenComponent} openComponent={this.state.openComponent}/>
		)
	}

	toggleOpenComponent = () => {
		this.setState({
			openComponent: !this.state.openComponent
		})
	}
}