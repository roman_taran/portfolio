import React from 'react'

/*style*/
import 'react-circular-progressbar/dist/styles.css'

/*custom*/
import Skill                from './Skill'


let Skills = ({ data }) => {

	let renderSkillItem = (item) => {
		return (
			item.map(skill =>
				<li className="skills_item" key={skill.id}>
					<Skill value={skill.level} name={skill.name}/>
				</li>
			)
		)
	};


	return (
		<ul className="skills">
			{
				data.map(skill => {
					return (
						<li className="skills_group" key={skill.id}>
							<h4 className="skills_title">{skill.name}</h4>
							<ul className="skills_list">
								{renderSkillItem(skill.skillItem)}
							</ul>
						</li>
					)
				})
			}
		</ul>
	)
};


export default Skills;