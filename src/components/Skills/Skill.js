import React, { Component } from 'react'
import CircularProgressbar  from 'react-circular-progressbar';
import Waypoint             from 'react-waypoint'


class Skill extends Component {
	constructor(props) {
		super();
		this.state = {
			levelStart: 0,
			level: props.value
		}

	}

	_handleWaypointEnter = (e) => {
		// console.log('+++++');
		this.setState({
			levelStart: this.state.level
		})
	};

	_handleWaypointLeave = (e) => {
		// console.log('-----');

		this.setState({
			levelStart: this.state.levelStart
		});
	};

	componentDidMount() {
		this.setState({
			levelStart: this.state.levelStart
		});

		this._handleWaypointEnter();
		this._handleWaypointLeave();
	}

	render() {
		const PathStyles = {
			path: {
				stroke: `rgba(62, 152, 199, ${this.props.value / 100})`
			}
		};
		return (
			<Waypoint scrollableAncestor={window}
					  onEnter={this._handleWaypointEnter}
					  onLeave={this._handleWaypointLeave}
			>
				<div className="skills_circle">
					<h5 className="skills_name">{this.props.name}</h5>
					<CircularProgressbar
						percentage={this.state.levelStart}
						textForPercentage={null}
						strokeWidth={15}
						initialAnimation={true}
						styles={PathStyles}
					/>
				</div>
			</Waypoint>
		)
	}

};

// function mapStateToProps(state) {
// 	return {
// 		color:
// 	}
// }
//
// let decorator = connect(mapStateToProps)

export default Skill;