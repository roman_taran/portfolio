import React from 'react'


export default function Arrow(props) {
	return (
		<div className={props.className}
			 onClick={props.onClick}>
			<button className="portfolio-slider_btn-inner">
				{props.children}
			</button>
		</div>
	);
}
