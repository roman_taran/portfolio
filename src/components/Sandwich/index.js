import React, { Component } from 'react'
import { connect }          from 'react-redux'

import {toggleMenu} from '../../AC'
import ToggleComponent from '../../decorators/toggleComponent'

class Sandwich extends Component {


	render() {
		const { open, toggleOpenComponent } = this.props;
		return (
			<div className={`sandwich ${open ? 'is-active' : ''}`}>
				<button className='sandwich_inner' onClick={toggleOpenComponent}>
					<span className="sandwich_top"> </span>
					<span className="sandwich_middle"> </span>
					<span className="sandwich_bottom"> </span>
				</button>
			</div>

		)
	}
}

function mapStateToProps(state) {
	return {
		open: state.openMenu
	}
}

const mapToDispatch = { toggleMenu };

const decorator = connect(mapStateToProps, mapToDispatch);

export default decorator(ToggleComponent(Sandwich));