import React, { Component } from 'react'

/*custom components*/
import User                 from '../User'

/*img*/
import bg                   from '../../assets/images/water.jpg'
import ArrowDown            from '../../assets/images/icon/arrow_down.svg'


export default class Hero extends Component {

	render() {
		const heroStyles = {
			backgroundImage: `url(${bg})`
		};

		return (
			<section className="hero" style={heroStyles}>
				<div className="hero_content">
					<div className="hero_content-bg"> </div>
					<User>
						{this.props.children}
					</User>
				</div>
				<div className="hero_btn">
					<a href="#0" className="btn-scroll">
						<ArrowDown wight={20} heght={20}/>
					</a>
				</div>
			</section>
		)
	}
}