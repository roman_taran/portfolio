import React from 'react'

function PortfolioSlide ({image, id, className}) {
	const styles = {
		backgroundImage: `url(${image})`
	};
    return (
			<div className={className}
				 style={styles}>
				<span>{id}</span>
			</div>
	);
}

export default PortfolioSlide;

