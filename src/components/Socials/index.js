import React     from 'react'
import PropTypes from 'prop-types'

/*img*/
import Github    from '../../assets/images/icon/github.svg'


const Socials = ({ className = '' }) => {

	return (
		<ul className={'socials ' + className}>
			<li className='socials_item'>
				<a href="#0">
					<Github width={25} height={25}/>
				</a>
			</li>
			<li className='socials_item'>
				<a href="#0">
					<Github width={25} height={25}/>
				</a>
			</li>
		</ul>
	);
};


export default Socials;

Socials.propTypes = {
	className: PropTypes.string
};
