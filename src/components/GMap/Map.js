import React from 'react'

import {withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps"


const MyMapComponent = withScriptjs(withGoogleMap((props) =>
	<GoogleMap
		defaultZoom={15}
		defaultCenter={{lat: 50.006242, lng: 36.221700}}
		defaultOptions={{styles: props.mapStyles}}
//		options={{zoomControlOptions: {position: google.maps.ControlPosition.LEFT_CENTER}}}
	>
		{props.isMarkerShown && <Marker position={{lat: 50.005959, lng: 36.236224}}/>}
	</GoogleMap>
));


export default MyMapComponent