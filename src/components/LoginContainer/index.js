import React              from 'react'
import PropTypes          from 'prop-types'
import CSSTransitionGroup from 'react-addons-css-transition-group'

/*custom*/
import LoginFront         from './LoginFront'
import LoginBack          from './LoginBack'


const LoginContainer = ({ backActive, handleClick }) => {

    return (
        <div className="flip">
            <CSSTransitionGroup
                transitionName="flip-animation"
                transitionEnterTimeout={1000}
                transitionLeaveTimeout={1000}>
                    { backActive ? <LoginBack handleClick={handleClick} key="0"/>
                        : <LoginFront key="1"/> }

            </CSSTransitionGroup>
        </div>
    );
};

export default LoginContainer;

LoginContainer.propTypes = {
    backActive: PropTypes.bool,
    handleClick: PropTypes.func.isRequired
};
