import React from 'react'
import { Link }             from 'react-router-dom'
// import PropTypes            from 'prop-types';

/*custom*/
import User                 from '../../components/User'
import Socials              from '../../components/Socials'


const LoginFront = () => {

		return (
			<div className="flip_front">
				<div className="flip_user">
					<User className='user--login'>Личный сайт веб разработчика</User>
				</div>
				<div className="flip_socials">
					<Socials/>
				</div>
				<div className="flip_btn-front">
					<Link className="flip_link" to={'/portfolio'}>
						<span>Портфолио</span>
					</Link>
					<Link className='flip_link' to={'/'}>
						 <span>Обо мне</span>
					</Link>
					<Link className="flip_link" to={'/blog'}>
						<span>Блог</span>
					</Link>
				</div>
			</div>
		);
};

export default LoginFront;

LoginFront.propTypes = {};
