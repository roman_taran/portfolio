import React, { Component } from 'react'
import PropTypes            from 'prop-types'


/*img*/
import LoginIcon            from '../../assets/images/icon/login.svg'
import PassIcon             from '../../assets/images/icon/password.svg'


export default class LoginBack extends Component {
    static propTypes = {
        handleClick: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            loginName: '',
            loginPass: '',
            formErrors: { loginPass: '', loginName: '' },
            loginNameValid: false,
            loginPassValid: false,
            checkHuman: false,
            checkHumanRadio: false,
            formValid: false
        };

        this.validClass = 'is-valid';
        this.inValidClass = 'is-invalid';
    };

    validateField = (fieldName, target) => {
        let {
            formErrors,
            loginPassValid,
            loginNameValid,
            checkHuman,
            checkHumanRadio
        } = this.state;

        switch ( fieldName ) {
            case 'loginPass' :
                loginNameValid = target.value.length >= 4;
                formErrors.loginPass = loginNameValid ? this.validClass : this.inValidClass;
                break;
            case 'loginName' :
                loginPassValid = target.value.length >= 6;
                formErrors.loginName = loginPassValid ? this.validClass : this.inValidClass;
                break;
            case 'checkHuman' :
                checkHuman = target.checked;
                break;
            case 'checkHumanRadio' :
                if ( target.value === 'noSubmit' ) {
                    checkHumanRadio = false;
                } else {
                    checkHumanRadio = true;
                }
                break;
            default:
                break;
        }

        this.setState({
            formErrors,
            loginPassValid,
            loginNameValid,
            checkHuman,
            checkHumanRadio
        }, this.validateForm);

    };

    validateForm = () => {
        const { loginNameValid, loginPassValid, checkHuman, checkHumanRadio } = this.state;
        this.setState({
            formValid: loginNameValid && loginPassValid && checkHuman && checkHumanRadio
        });
    };

    handleChange = (e) => {
        const target = e.target;
        const { name, value } = target;
        this.setState({ [ name ]: value },
            () => { this.validateField(name, target) });
    };

    render() {
        const { handleClick } = this.props;
        const { formErrors } = this.state;
        return (
            <div className="flip_back">
                <div className="flip_back-title">
                    <h3 className="title line title-medium">Авторизоваться</h3>
                </div>
                <form className="form flip_form" id="form-login">
                    <div className="flip_inner">
                        <div className="form_block">
                            <label htmlFor="loginName" className={`input-icon ${formErrors.loginName}`}>
                                <span><LoginIcon wight={20} height={20}/></span>
                                <input type="text" name='loginName'
                                       placeholder="Логин"
                                       className="input"
                                       onChange={this.handleChange}/>
                            </label>
                        </div>
                        <div className="form_block">
                            <label htmlFor="loginPass" className={`input-icon ${formErrors.loginPass}`}>
                                <span><PassIcon wight={20} height={20}/></span>
                                <input type="password" name='loginPass'
                                       placeholder="Пароль"
                                       className="input"
                                       onChange={this.handleChange}/>
                            </label>
                        </div>
                        <div className="form_check">
                            <label className='checkbox'>
                                <input type="checkbox" name='checkHuman'
                                       onChange={this.handleChange}/>
                                <span className='checkbox_icon'>Я человек </span>
                            </label>
                        </div>
                        <div className="form_check">
                            <span className="form_check-title">Вы точно не работ?</span>
                            <label className='radio'>
                                <input type="radio" name='checkHumanRadio'
                                       onChange={this.handleChange}/>
                                <span>Да</span>
                            </label>
                            <label className='radio'>
                                <input type="radio" name='checkHumanRadio'
                                       value="noSubmit"
                                       onChange={this.handleChange}/>
                                <span>Не уверен</span>
                            </label>
                        </div>
                    </div>
                </form>
                <div className="flip_btn-back">
                    <button className="flip_btn-item flip_link"
                            onClick={handleClick}>
                        <span>На главную </span>
                    </button>
                    <button type='submit' form="form-login"
                            disabled={!this.state.formValid}
                            className="flip_btn-item flip_link">
                        Войти
                    </button>
                </div>
            </div>
        );
    }
}

LoginBack.propTypes = {};
