import React, { Component }    from 'react'
//import { TimelineMax, Linear } from 'gsap'


export default class Button extends Component {

	constructor(props) {
		super();

	}

	handleClick = (event) => {
		let circle = document.getElementById('js-ripple'),
			ripple = document.getElementsByClassName('js-ripple'),
			e = event.nativeEvent;



		for ( let i = 0; i < ripple.length; i++ ) {
//			let tl = new TimelineMax();
			let x = e.offsetX;
			let y = e.offsetY;
			let w = e.target.offsetWidth;
			let h = e.target.offsetHeight;
			let offsetX = Math.abs((w / 2) - x);
			let offsetY = Math.abs((h / 2) - y);
			let deltaX = (w / 2) + offsetX;
			let deltaY = (h / 2) + offsetY;
			let scale_ratio = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));

//			tl.fromTo(ripple[i], 0.75, {
//				x: x,
//				y: y,
//				transformOrigin: '50% 50%',
//				scale: 0,
//				opacity: 1,
//				ease: Linear.easeIn
//			}, {
//				scale: scale_ratio,
//				opacity: 0
//			});
		}


	};

	render() {
		return (
			<span className='btn-ripple'>
				<div id="js-ripple-btn" {...this.props}
				   onClick={e => this.handleClick(e)}>
					<svg className="ripple-obj" id="js-ripple">
						<use width="100" height="100" xlinkHref="#ripply-scott" className="js-ripple"/>
					</svg>
					{this.props.children}
				</div>
			</span>
		);
	}

}

