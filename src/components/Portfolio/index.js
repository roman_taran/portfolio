import React, { Component } from 'react'
import Waypoint             from 'react-waypoint'

/*custom*/
import PortfolioSlider      from '../PortfolioSlider'

/*img*/
import SvgWorks             from '../../assets/images/icon/works_header.svg'


export default class PortfolioSection extends Component {
	constructor(props) {
		super();

		this.state = {
			titleAnimate: false
		}
	}

	_handleWaypointEnter = (e) => {
		this.setState({
			titleAnimate: true
		})
	};

	_handleWaypointLeave = (e) => {
		this.setState({
			titleAnimate: false
		})
	};

	render() {
		const activeClass = 'is-active';
		let { titleAnimate } = this.state;
		return (
			<section className="portfolio">
				<div className="container-fluid">
					<div className="portfolio_title">

						<Waypoint onEnter={this._handleWaypointEnter}
								  onLeave={this._handleWaypointLeave}
						>
							<div>
								<div className={`portfolio_title-bg ${titleAnimate ? activeClass : ''}`}>
									<SvgWorks height={150} width={550}/>
								</div>
								<h2 className="title-big">Мои работы</h2>
							</div>

						</Waypoint>
					</div>

				</div>
				<PortfolioSlider/>
			</section>
		)
	}
}