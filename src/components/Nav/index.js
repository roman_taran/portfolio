import React from 'react'
import { NavLink }          from 'react-router-dom'


let Nav = ({ className }) => {

	// const { className } = this.props;

	return (
		<nav className={`nav js-nav ${className}`}>
			<ul className="nav_list">
				<li className="nav_item">
					<NavLink to={`/portfolio`}>
							<span className="nav_link" data-text="Мои работы">
								<span>Мои работы</span>
							</span>
					</NavLink>
				</li>
				<li className="nav_item">
					<NavLink to={`/blog`}>
							<span className="nav_link" data-text="Блог">
								<span>Блог</span>
							</span>
					</NavLink>
				</li>
				<li className="nav_item">
					<NavLink to={`/`}>
							<span className="nav_link" data-text="Обо мне">
								<span>Обо мне</span>
							</span>
					</NavLink>
				</li>
				<li className="nav_item">
					<NavLink to={`/login`}>
							<span className="nav_link" data-text="Авторизация">
								<span>Авторизация</span>
							</span>
					</NavLink>
				</li>
			</ul>
		</nav>
	);
}

export default Nav;




