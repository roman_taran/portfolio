import React, { Component } from 'react'
import { connect }          from 'react-redux'
import CSSTransitionGroup  from 'react-addons-css-transition-group'


/*custom*/
import Nav                  from './index'
import ToggleComponent      from "../../decorators/toggleComponent"

class ContainerNav extends Component {


    render() {
    	const { open } = this.props;
        const transitionClassName = {
            enter: 'nav_animation-enter',
            enterActive: 'nav_animation-enterActive',
            leave: 'nav_animation-leave',
            leaveActive: 'nav_animation-leaveActive'
        }

        return (
            <CSSTransitionGroup
                transitionName={transitionClassName}
                transitionEnterTimeout={1200}
                transitionLeaveTimeout={600}>
                { open ?
                    <div className="nav-wrapper is-active">
                        <Nav className='nav--coll' />
                    </div> :
                    null
                }

            </CSSTransitionGroup>
        )
    }
}

function mapStateToProps(state) {
	return {
		open: state.openMenu
	}
}
const decorator = connect(mapStateToProps);

export default decorator(ToggleComponent(ContainerNav))