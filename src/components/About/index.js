import React from 'react'

/*custom*/
import Skills               from '../Skills'

/*img*/
import userImg              from '../../assets/images/user-img.jpg'


let About = (props) => {

	return (
		<section className="about">
			<div className="about_left">
				<div className="about_left-title">
					<h3 className=" title title-big line">Обо мне</h3>
				</div>
				<div className="user-content">
					<div className="user-content_img">
						<img src={userImg} alt=""/>
					</div>
					<div className="user-content_text">
						<h4 className='user-content_title title-medium title line'>Кто я</h4>
						<div className="user-content_info">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci atque ducimus eos
								inventore ipsa ipsam magni molestias necessitatibus provident rem. Aut deserunt
								dolorum Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab architecto
								impedit ipsam necessitatibus sapiente sit. iure magnam, pariatur porro tenetur vero.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, facere?</p>
						</div>

					</div>
				</div>
			</div>
			<div className="about_right">
				<div className="about_right-inner">
					<div className="about_right-head">
						<div className="about_right-title">
							<h4 className='line title title-medium'>ЧЕМ Я МОГУ БЫТЬ ВАМ ПОЛЕЗЕН</h4>
						</div>
						<div className="about_right-content">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium at culpa
								delectus eos
								facilis id illo ipsa ipsam ipsum, necessitatibus optio quaerat, quibusdam,
								recusandae reiciendis
								saepe sequi soluta temporibus tenetur!</p>
						</div>
					</div>
					<div className="about_right-skills">
						<Skills data={props.skillData} />
					</div>
				</div>
			</div>
		</section>
	)
};

export default About;