export const SliderImages = [
	{
		'id': '1',
		'title': 'Voluptate est officia 1',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'https://arraythemes.com/wp-content/uploads/2017/03/atomic-blog-image2-1400x1010.jpg'
	},
	{
		'id': '2',
		'title': 'Launch a bold business and portfolio site with Atomic 2',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'http://www.rodeodesign.com/wordpress/wp-content/uploads/2015/11/gady-1.jpg'
	},
	{
		'id': '3',
		'title': 'Site Portfolio Concept Sketch freebie 3',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'https://www.sketchappsources.com/resources/source-image/kozaderov-site-0.png'
	},
	{
		'id': '4',
		'title': 'Personal Portfolio Site Landing Page 4',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'https://i.pinimg.com/originals/ee/de/60/eede60ef414535089ac841130d976c01.jpg'
	},
	{
		'id': '5',
		'title': '10 Characteristics of Excellent Portfolio Sites 5',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'https://www.webdesignerdepot.com/cdn-origin/uploads/2008/12/mash.jpg'
	},
	{
		'id': '6',
		'title': 'portfolio page template 6',
		'link': '#0',
		'technology': [
			{
				'id': '1',
				'name': 'html'
			},
			{
				'id': '2',
				'name': 'css'
			}
		],
		'images': 'https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/startuprr-html-multipurpose-portfolio-site-template.jpg'
	},
];